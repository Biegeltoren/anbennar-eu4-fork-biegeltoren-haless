#Yingshi Events

namespace = yingshi

#Lies and Slander 

country_event = {
	id = yingshi.1
	title = yingshi.1.t
	desc = yingshi.1.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #Perform the attack
		name = yingshi.1.a
		
	Y01 = {
		country_event = {
			id = yingshi.2
			days = 15
			}
		}
		
	Y02 = {
		country_event = {
			id = yingshi.3
			days = 1
			}
		}
	}
}

		

#Lies And Slander 2
country_event = {
	id = yingshi.2
	title = yingshi.2.t
	desc = yingshi.2.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes
	
	trigger = {
		always = yes
	}
	
	
	option = { #Declare War on Jingqui
		name = yingshi.2.a
		ai_chance = {
			factor = 100
					  }
					
	Y01 = {
			declare_war_with_cb = {
				who = Y02
				casus_belli = cb_humiliate_rotw
									}
		  }
	add_manpower = -2
	}
					  
	option = { #Back Down
		name = yingshi.2.b
		ai_chance = {
			factor = 0
					 }
					 
	add_prestige = -20
	add_manpower = -2
	}
	
}

#Lies and Slander 3
country_event = {
	id = yingshi.3
	title = yingshi.3.t
	desc = yingshi.3.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #wtf happened
		name = yingshi.3.a
		
	add_prestige = -20
		}
	}

#A Keeper of Secrets
country_event = {
	id = yingshi.4
	title = yingshi.4.t
	desc = yingshi.4.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #Neat
		name = yingshi.4.a
		}
}
		
#Oni Overture
country_event = {
	id = yingshi.5
	title = yingshi.5.t
	desc = yingshi.5.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = {  #Accept
		name = yingshi.5.a
		ai_chance = {
			factor = 100
				}
		
		FROM = {
			country_event = {
				id = yingshi.6
				}
			}
		create_subject = {
			subject_type = "tributary_state"
			subject = Y10
			}
	}
	
		option = {  #Deny
		name = yingshi.5.b
		ai_chance = {
			factor = 0
				}
				
		add_prestige = -20
	}
}

#Oni Overture 2
country_event = {
	id = yingshi.6
	title = yingshi.6.t
	desc = yingshi.6.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
		option = {  #Neat
		name = yingshi.6.a

		add_country_modifier = {
			name = yingshi_oni_tutelage
			duration = 9125
			}
		}
}
	
#The Contract Fulfilled
country_event = {
	id = yingshi.7
	title = yingshi.7.t
	desc = yingshi.7.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #Success
	name = yingshi.7.a
	
	add_country_modifier = {
		name = yingshi_oni_bounty
		duration = 3650
		}
	add_treasury = 100
	}
}

#The Contract Fulfilled 2
country_event = {
	id = yingshi.8
	title = yingshi.8.t
	desc = yingshi.8.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #rip
	name = yingshi.8.a
	
	kill_advisor = random
	}
}

#Anjiang Murder
country_event = {
	id = yingshi.9
	title = yingshi.9.t
	desc = yingshi.9.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #here we go
	name = yingshi.9.a
	
	Y04 = {
		country_event  = {
			id = yingshi.13
			days = 3
			}
		}
	}
}

#Best Things 1 - Preparation
country_event = {
	id = yingshi.10
	title = yingshi.10.t
	desc = yingshi.10.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #ok
	name = yingshi.10.a
	}
}

#Best Things 2 - Rival is Murdered
country_event = {
	id = yingshi.11
	title = yingshi.11.t
	desc = yingshi.11.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #rip ruler :c
	name = yingshi.11.a
	
	kill_ruler = yes
	
	hidden_effect = {
		FROM = { 
			country_event = {
				id = yingshi.12
				days = 2
				}
			}
		}
	}
}


#Best Things 3 - Victorious
country_event = {
	id = yingshi.12
	title = yingshi.12.t
	desc = yingshi.12.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #ok
	name = yingshi.12.a
	
	add_prestige = 30
	}
}

#Anjiang Choice
country_event = {
	id = yingshi.13
	title = yingshi.13.t
	desc = yingshi.13.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #trust the emissaries! 
	name = yingshi.13.a
	
	ai_chance = {
		factor = 100
			}
	
		add_trust = {
			who = Y10	
			value = -30
			mutual = yes
				}
				
		reverse_add_opinion = {
			who = Y10
			modifier = anjiang_going_alone
				}
				
		set_ai_attitude = {
			attitude = attitude_hostile
			who = Y10
			locked = no
				}
				
		add_trust = {
			who = Y06	
			value = -30
			mutual = yes
				}
				
		reverse_add_opinion = {
			who = Y06
			modifier = anjiang_going_alone
				}
				
		set_ai_attitude = {
			attitude = attitude_hostile
			who = Y06
			locked = no
				}
				
		add_trust = {
			who = Y07	
			value = -30
			mutual = yes
				}
				
		reverse_add_opinion = {
			who = Y07
			modifier = anjiang_going_alone
				}
				
		set_ai_attitude = {
			attitude = attitude_hostile
			who = Y07
			locked = no
				}
				
		add_trust = {
			who = Y08	
			value = -30
			mutual = yes
				}
				
		reverse_add_opinion = {
			who = Y08
			modifier = anjiang_going_alone
				}
				
		set_ai_attitude = {
			attitude = attitude_hostile
			who = Y08
			locked = no
				}
	
		add_prestige = 50
		
		every_country = {
			limit = {
				OR = {
					tag = Y10
					tag = Y06
					tag = Y07
					tag = Y08
					}
				}
			country_event = {
				id = yingshi.14
				days = 10
				}
			}
		
		}
		
	option = { #Player opt out
		name = yingshi.13.b
		
		ai_chance = {
			factor = 0
				}
				
		add_stability = -1
		
		}
		
	
	}


#Anjiang Aftermath
country_event = {
	id = yingshi.14
	title = yingshi.14.t
	desc = yingshi.14.d
	picture = ASSASSINATION_eventpicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #wtf
	name = yingshi.14.a
	
	add_prestige = -10
	
	
	}
}
#Steal The Vote 1
country_event = {
	id = yingshi.15
	title = yingshi.15.t
	desc = yingshi.15.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #Rig It
		name = yingshi.15.a
				
		add_treasury = -150
		
		Y06 = {
			country_event = {
				id = yingshi.16
				days = 150
			}
		}
		
	}
}

#Steal The Vote 2
country_event = {
	id = yingshi.16
	title = yingshi.16.t
	desc = yingshi.16.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #Accept Results
		name = yingshi.16.a
		
		ai_chance = {
			factor = 100
			}
		
	
		
		
	Y10 = {
		country_event = {
			id = yingshi.17
			}
		inherit = Y06
			
		}
		
	}
	option = { #Player Opt Out
		name = yingshi.15.b
		
		ai_chance = {
			factor = 0
			}
			
		add_stability = -1
	}
}
#Steal The Vote 3
country_event = {
	id = yingshi.17
	title = yingshi.17.t
	desc = yingshi.17.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #cool
		name = yingshi.17.a
						
	}
}

#Steal The Vote 4 revolt
country_event = {
	id = yingshi.18
	title = yingshi.18.t
	desc = yingshi.18.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #oh shit oh fuck
		name = yingshi.18.a
		
		4861 = {
			spawn_rebels = {
				type = seperatist_rebels
				size = 3
				}
			}
			
						
	}
}
#Corrupt The Patrol 1
country_event = {
	id = yingshi.19
	title = yingshi.19.t
	desc = yingshi.19.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #let's do it
		name = yingshi.19.a
		
		Y08 = {
			country_event = {
				id = yingshi.20
				days = 730
			}
		}	
	}
}
#Corrupt The Patrol 2
country_event = {
	id = yingshi.20
	title = yingshi.20.t
	desc = yingshi.20.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #rip
		name = yingshi.20.a
		ai_chance = {
			factor = 100
			}
		
		FROM = { 
			inherit = ROOT
			country_event = {
				id = yingshi.21
			}
		}
		Y07 = {
			country_event = {
				id = yingshi.22
				days = 40
			}
		}
		4855 = { add_permanent_claim = Y07 }
		4859 = { add_permanent_claim = Y07 }
	}	
	option = { #player opt out
		name = yingshi.20.b
		ai_chance = {
			factor = 0
			}
		add_stability = -1
	}
}

#Corrupt The Patrol 3
country_event = {
	id = yingshi.21
	title = yingshi.21.t
	desc = yingshi.21.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #cool
		name = yingshi.21.a
		
		4950 = { add_permanent_claim = ROOT }
		4958 = { add_permanent_claim = ROOT }
	}	
}
#Corrupt The Patrol 4
country_event = {
	id = yingshi.22
	title = yingshi.22.t
	desc = yingshi.22.d
	picture = ANGRY_MOB_eventPicture
	
	is_triggered_only = yes

	trigger =	{
		always = yes
	}
	
	option = { #rip
		name = yingshi.22.a
		ai_chance = {
			factor = 100
			}
			

		
		declare_war = Y10
					
	}	
	option = { #player opt out
		name = yingshi.22.b
		ai_chance = {
			factor = 0
			}
		add_stability = -1
	}
}

##Best Things Offensive Events

#Assassinate Advisor
country_event = {
	id =  yingshi.23
	title = yingshi.23.t
	desc = yingshi.23.d
	picture = FEAST_eventPicture
	
	trigger = {
		any_rival_country = {
			has_spy_network_from = {
				who = ROOT
				value = 60
			}
		}
		NOT = { has_country_modifier = yingshi_best_things_cd }
		has_country_flag = allow_yingshi_events
	}
	
	is_triggered_only = yes

#	mean_time_to_happen = {
#		days = 180
#	}
	
	immediate = {
	}
	
	option = { #Take The Shot
		name = yingshi.23.a
		ai_chance = 67
		
		random_rival_country = {
			limit = {
				has_spy_network_from = {
					who = ROOT
					value = 60
				}
			}
			THIS = { 
				country_event = { 
					id = yingshi.8 
				}
			}
			add_spy_network_from = {
				who = ROOT
				value = -60
			}
		}
		
		add_adm_power = 50
		add_dip_power = 50
		add_mil_power = 50
		
		if = {
			limit = {
				has_idea_group = espionage
			}
			
			add_country_modifier = {
				name = yingshi_best_things_cd
				duration = 1820
			}
		}
		else = { 
			add_country_modifier = {
				name = yingshi_best_things_cd
				duration = 3640
			}
		}
	}	



	option = { #Wait for a better opportunity.
		name = yingshi.23.b
		ai_chance = {
			factor = 33
			}

		}
}